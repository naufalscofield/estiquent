import {
  StyleSheet,
  Platform
} from 'react-native'

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
    flex: 1,
    // flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  changeBtn: {
    height: 100,
    width: 200,
    marginBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'powderblue'
  },
  mainmenuText: {
    color: 'powderblue',
    fontSize: 40,
    textAlign: 'center',
    color: 'powderblue',
  }
});
