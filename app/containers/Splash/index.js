/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  ImageBackground,
} from 'react-native';

import { StackActions, NavigationActions } from 'react-navigation';

import styles from './style';

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount = () => {
    setTimeout( () => {
      this._reset()
    },3000)
  }
  //
  _reset(){
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Login' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    return (
      <ImageBackground
        source={require('./../../img/splash.png')}
        style={{width: '100%', height:'100%'}}>
      </ImageBackground>
    );
  }

}

  App.navigationOptions = {
  header: null
  }
