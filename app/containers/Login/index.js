/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  ToastAndroid
} from 'react-native';
import styles from './style';
import Metrics from '../../utils/Metrics';
import Api from '../../config/Api'

type Props = {};
export default class Login extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      loading: false
    }
  }

  _login(){
    let params = {
      username : this.state.username,
      password : this.state.password
    }

    this.setState({ loading: true })
    Api.post('login', params).then(resp => {
      if (resp.httpStatus == 200) {
        this.setState({ loading: false })
        this.props.navigation.navigate('Matrix')
      } else {
        this.setState({ loading: false })
        ToastAndroid.show(resp.data.message, ToastAndroid.SHORT);
      }
    }).catch((e) => {
      console.log(e)
    })
  }

  render() {
    return (
        <ScrollView style={styles.container}>
          <View style={styles.logo}>
            <Image
              style={styles.stretch}
              source={require('./../../img/logo.png')}
            />
          </View>
          <View style={styles.input}>
            <TextInput
              value={this.state.username}
              onChangeText={(value) => this.setState({ username: value }) }
              placeholder='Your Username'
              underlineColorAndroid='orange'
            />
            <TextInput
              value={this.state.password}
              onChangeText={(value) => this.setState({ password: value }) }
              placeholder='Your Password'
              underlineColorAndroid='orange'
              secureTextEntry={true}
            />
            <TouchableOpacity
              style={styles.addButton}
              onPress={ () => this._login() }>
            <Text
              style={styles.textLoginButton}>
              LOGIN
            </Text>
          </TouchableOpacity>
          </View>
        </ScrollView>
    );
  }
}

  Login.navigationOptions = {
  header: null
  }
