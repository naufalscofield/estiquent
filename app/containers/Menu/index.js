/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
} from 'react-native';
import styles from './style';
import Metrics from '../../utils/Metrics';


type Props = {};
export default class Login extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    }
  }

  _login(){

  }

  render() {
    return (
        <ScrollView style={styles.container}>
          <View style={styles.logo}>
            <Image
              style={styles.stretch}
              source={require('./../../img/logo.png')}
            />
          </View>
        </ScrollView>
    );
  }
}

  Login.navigationOptions = {
  header: null
  }
