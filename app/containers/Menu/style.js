import {
  StyleSheet,
  Platform
} from 'react-native'
import Metrics from '../../utils/Metrics';

export default StyleSheet.create({
  container: {
    // justifyContent: 'center',
    backgroundColor: '#F5FCFF',
    flex: 1,
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#fff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  changeBtn: {
    height: 100,
    width: 200,
    marginBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'powderblue'
  },
  mainmenuText: {
    color: 'powderblue',
    fontSize: 40,
    textAlign: 'center',
    color: 'powderblue',
  },
  stretch: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain',
    margin: 20,
    marginTop: 50,
    marginHorizontal: 100,
    // backgroundColor: 'red',
  },
  logo: {
    width: Metrics.screenWidth,
    height: Metrics.pixelRatio * 100,
    // backgroundColor: 'red',
  },
  input: {
    // width: Metrics.screenWidth,
    // height: Metrics.pixelRatio * 120,
    marginHorizontal: 50,
    flex: 1,
  },
  addButton: {
    backgroundColor: '#0E4379',
    height: 60,
    borderRadius: 5,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  textAddButton: {
    color: 'white',
  },
});
