/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
} from 'react-native';
import styles from './style';
import Metrics from '../../utils/Metrics';


type Props = {};
export default class Matrix extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    }
  }

  _login(){

  }

  render() {
    return (
        <ScrollView style={styles.container}>
          <View style={styles.header}>
            <Image
              style={styles.stretch}
              source={require('./../../img/logo.png')}
            />
            <View style={styles.menuTitle}>
              <Text
                style={styles.textHeader}>
                MyCompany
              </Text>
            </View>
          </View>

          <View style={styles.content}>
            <TouchableOpacity
              style={styles.profileButton}>
            </TouchableOpacity>
          </View>
        </ScrollView>
    );
  }
}

  Matrix.navigationOptions = {
  header: null
  }
