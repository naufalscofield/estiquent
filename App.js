/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Splash from './app/containers/Splash'
import Login from './app/containers/Login'
import NWC from './app/containers/NWC'
import Menu from './app/containers/Menu'
import MyCompany from './app/containers/MyCompany'
import Matrix from './app/containers/Matrix'

// const instructions = Platform.select({
//   ios: 'Press Cmd+R to reload,\n' +
//     'Cmd+D or shake for dev menu',
//   android: 'Double tap R on your keyboard to reload,\n' +
//     'Shake or press menu button for dev menu',
// });

type Props = {};
const RootStack = createStackNavigator(
  {
    Splash: Splash,
    Login: Login,
    NWC: NWC,
    Menu: Menu,
    MyCompany: MyCompany,
    Matrix: Matrix,
  },
  {
    initialRouteName: 'Matrix',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}
