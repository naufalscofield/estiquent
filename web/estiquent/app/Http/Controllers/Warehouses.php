<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\User;

class Warehouses extends Controller
{
    public function index()
    {
        // return true;
        $id = session('id_user');
        // dd($id)
        $data = User::find($id);
        $company_code = $data->company_code;
        return view ('admin/warehouses',["company_code" => $company_code]);
    }

    public function tes()
    {
        echo 'tes'; die;
    }
}
