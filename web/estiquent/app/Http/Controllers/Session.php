<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Session extends Controller
{
    public function index(Request $request, $id, $pw)
    {
        $request->session()->set('id_user',$id);
        $request->session()->set('password',$pw);
        return redirect('/profile');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/');
    }
}
