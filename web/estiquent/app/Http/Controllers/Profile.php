<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class Profile extends Controller
{
    public function index()
    {
        $id = session('id_user');
        $pw = session('password');
        return view('admin/profile',["id"=>$id],["pw"=>$pw]);
    }
}
