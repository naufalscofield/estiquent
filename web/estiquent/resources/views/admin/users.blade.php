@extends('admin/master')

@section('title','Estiquent')
    
@section('content')
<style>
#myInput {
    /* background-image: url('/css/filter.png'); Add a search icon to input */
    background-position: 10px 12px; /* Position the search icon */
    background-repeat: no-repeat; /* Do not repeat the icon image */
    width: 30%; /* Full-width */
    color: grey;
    font-size: 16px; /* Increase font-size */
    padding: 1px 1px 12px 1px; /* Add some padding */
    border: 2px solid #9999ff; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
}
</style>
<div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="/img/logo.png" style="width:200px;height:50px" alt=""></center>
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="/admin">
                        <i class="fa fa-tachometer"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="/profile">
                        <i class="fa fa-user"></i>
                        <p>Profile</p>
                    </a>
                </li>
                <li class="active">
                    <a href="/users">
                        <i class="fa fa-users"></i>
                        <p>Users</p>
                    </a>
                </li>
                <li>
                    <a href="/warehouses">
                        <i class="fa fa-home"></i>
                        <p>Warehouses</p>
                    </a>
                </li>
                <li>
                    <a href="/destinations">
                        <i class="fa fa-map-marker"></i>
                        <p>Destinations</p>
                    </a>
                </li>
                <li>
                    <a href="/transactions">
                        <i class="fa fa-exchange"></i>
                        <p>Transactions</p>
                    </a>
                </li>
                <li>
                    <a href="/costs">
                        <i class="fa fa-usd"></i>
                        <p>Costs</p>
                    </a>
                </li>
				<li>
                    <a href="/company">
                        <i class="fa fa-building"></i>
                        <p>MyCompany</p>
                    </a>
                </li>
				<li>
                    <a href="/logs">
                        <i class="fa fa-history"></i>
                        <p>Logs</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Users</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="/logout">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <center>
                                <label for=""><i>Filter Role</i></label>
                                <input type="hidden" id="company_code" value="{{ $company_code }}">
                                    <select id="myInput" onkeyup="filter()">
                                        <option selected value="">All</option>
                                        <option value="admin">Admin</option>
                                        <option value="user">User</option>
                                    </select>
                                </center>    
                                <h4 class="title"><button class="btn btn-success btn-fill" type="button" id="btn_input" data-toggle="modal" data-target="#addUser"><i class="fa fa-plus"></i></button></h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <th width="10"><center><b>No</b></center></th>
                                        <th width="20"><center><b>Name</b></center></th>
                                        <th width="20"><center><b>Birthplace/date</b></center></th>
                                        <th width="20"><center><b>Email</b></center></th>
                                        <th width="20"><center><b>Role</b></center></th>
                                        <th width="20"><center><b>Actions</b></center></th>
                                    </thead>
                                    <tbody id="table-row">
                                           <!-- -->
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="addUser" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Add User</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="add">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Company Code</label>
                                        <input type="text" required class="form-control border-input" disabled placeholder="Company Code" value="{{ $company_code }}" name="first_name">
                                    </div>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" required id="first_name" class="form-control border-input" placeholder="First Name" value="" name="first_name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" required id="last_name" class="form-control border-input" placeholder="Last Name" value="" name="last_name">
                                    </div>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Birth Place</label>
                                        <input type="text" required id="birth_place" class="form-control border-input" placeholder="Birth Place" name="birth_place">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Birth Date</label>
                                        <input type="date" required id="birth_date" class="form-control border-input" placeholder="Birth Date" name="birth_date">
                                    </div>
                                </div>
                            </div>
        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" required id="username" class="form-control border-input" placeholder="Username" value="" name="username">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" required id="password" class="form-control border-input" placeholder="Password" value="" name="password">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Role</label>
                                        <select name="role" id="role" class="form-control border-input">
                                            <option value="user">User</option>
                                            <option value="admin">Admin</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" required id="email" class="form-control border-input" placeholder="Email" value="" name="email">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Photo</label>
                                        <input type="file" required id="photo" class="form-control border-input" placeholder="Photo" value="" name="photo">
                                    </div>
                                    {{-- <div class="form-group">
                                        <label>Gender</label>
                                        <input type="radio" required id="gender" class="form-control border-input" placeholder="Gender" value="male" name="Gender">Male
                                        <input type="radio" required id="gender" class="form-control border-input" placeholder="Gender" value="female" name="Gender">Female
                                    </div> --}}
                                </div>
                            </div>
        
                            <div class="text-center">
                                {{ csrf_field() }}
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success btn-fill btn-wd">Add User</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
</div>
    <script>

////////////////////////////////////// functions //////////////////////////////////////////////////////////////
        function filter() {
        // Declare variables 
            var input, filter, table, tr, td, i;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("example");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[4];
                if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
                } 
            }
        }
        function getAll() {
            var company_code = $("#company_code").val();
            console.log(company_code)
            $.get("http://localhost:8000/api/users/" + company_code, function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
            var t = $('#example').DataTable();
            no++;
            t.row.add( [
                "<center>"+no+"</center>",
                "<center>"+element.first_name+" "+element.last_name+"</center>",
                "<center>"+element.birth_place+" "+element.birth_date+"</center>",
                "<center>"+element.email+"</center>",
                "<center>"+element.role+"</center>",
                "<center><a id='btn_delete' data-id="+element.id+" class='btn btn-danger'><i class='fa fa-trash'></i></a></center>"
            ] ).draw( false );
                    });
                }
                else {
                    alert('Load Data Failed!')
                }
            });
        }

        function clearTable() {
            var table = $('#example').DataTable();
             table
                .clear()
                .draw();
        }

 /////////////////////////////////////// document ready functions ///////////////////////////////////////////////

        $(document).ready(function() {
            $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
                ],
            } );

            getAll()

            
            $(document).on('submit', '#add' ,function(){
                var first_name          = $('#first_name').val();
                var last_name           = $('#last_name').val();
                var birth_place         = $('#birth_place').val();
                var birth_date          = $('#birth_date').val();
                var username            = $('#username').val();
                var password            = $('#password').val();
                var role                = $('#role').val();
                var email               = $('#email').val();
                var company_code        = $('#company_code').val();
                var gender              = $('#gender').val();

                $.post("http://localhost:8000/api/insert-user", {
                    first_name: first_name,
                    last_name: last_name,
                    birth_place: birth_place,
                    birth_date: birth_date,
                    username: username,
                    password: password,
                    role: role,
                    email: email,
                    company_code: company_code,
                    photo: 'default.jpg',
                    gender: 'male'
                    },
                    function(data, status){
                    if (status) {
                        // TempData["Success"] = "Success";
                        toastr.success('Insert Data Success')
                        clearTable()
                        $('#addUser').modal('hide')

                        $('#first_name').val('');
                        $('#last_name').val('');
                        $('#birth_place').val('');
                        $('#birth_date').val('');
                        $('#username').val('');
                        $('#password').val('');
                        $('#role').val('');
                        $('#email').val('');
                        getAll()

                    }
                    else {
                        toastr.error('Insert Data Failed!')
                    }
                });
            });
            
            $(document).on('click', '#btn_delete' ,function(){
                // console.log('tes')
                var id = $(this).data("id");
                jQuery.ajax({
                url: 'http://localhost:8000/api/delete-user/' + id,
                type: 'DELETE',
                success: function(data) {
                    clearTable()
                    getAll();
                    toastr.info('Delete Data Success!')
                    }
                });
            });
            
        } );        
    </script>
@endsection