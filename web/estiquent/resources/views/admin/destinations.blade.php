@extends('admin/master')

@section('title','Estiquent')
    
@section('content')
<div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="/img/logo.png" style="width:200px;height:50px" alt=""></center>
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="/admin">
                        <i class="fa fa-tachometer"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="/profile">
                        <i class="fa fa-user"></i>
                        <p>Profile</p>
                    </a>
                </li>
                <li>
                    <a href="/users">
                        <i class="fa fa-users"></i>
                        <p>Users</p>
                    </a>
                </li>
                <li>
                    <a href="/warehouses">
                        <i class="fa fa-home"></i>
                        <p>Warehouses</p>
                    </a>
                </li>
                <li class="active">
                    <a href="/destinations">
                        <i class="fa fa-map-marker"></i>
                        <p>Destinations</p>
                    </a>
                </li>
                <li>
                    <a href="/transactions">
                        <i class="fa fa-exchange"></i>
                        <p>Transactions</p>
                    </a>
                </li>
                <li>
                    <a href="/costs">
                        <i class="fa fa-usd"></i>
                        <p>Costs</p>
                    </a>
                </li>
				<li>
                    <a href="/company">
                        <i class="fa fa-building"></i>
                        <p>MyCompany</p>
                    </a>
                </li>
				<li>
                    <a href="/logs">
                        <i class="fa fa-history"></i>
                        <p>Logs</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Destinations</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="/logout">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><button class="btn btn-success btn-fill" type="button" id="btn_input" data-toggle="modal" data-target="#addDestination"><i class="fa fa-plus"></i></button></h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                    <input type="hidden" id="company_code" value="{{ $company_code }}">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <th width="10"><center><b>No</b></center></th>
                                        <th width="20"><center><b>Destination Code</b></center></th>
                                        <th width="20"><center><b>Destination Name</b></center></th>
                                        <th width="20"><center><b>Destination Location</b></center></th>
                                        <th width="20"><center><b>Actions</b></center></th>
                                    </thead>
                                    <tbody id="table-row">
                                           <!-- -->
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="addDestination" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Add Destination</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="add">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Company Code</label>
                                        <input type="text" disabled id="company_code" class="form-control border-input" placeholder="Company" value="{{ $company_code }}">
                                    </div>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Destination Code</label>
                                        <input type="text" required id="code" class="form-control border-input" placeholder="Destination Code" value="" name="code">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Destination Name</label>
                                        <input type="text" required id="name" class="form-control border-input" placeholder="Destination Name" name="name">
                                    </div>
                                </div>
                            </div>
        
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>City Location</label>
                                        <input type="text" required id="location" class="form-control border-input" placeholder="City Location" value="" name="location">
                                    </div>
                                </div>
                            </div>
        
                            <div class="text-center">
                                {{ csrf_field() }}
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success btn-fill btn-wd">
                                    Add Destination
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

            <div id="editDestination" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Edit Destination</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_id">
                                    <!-- -->    
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" id="input_company">
                                    <!-- -->    
                                    </div>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_code">
                                    <!-- --> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="input_name">
                                    <!-- -->
                                    </div>
                                </div>
                            </div>
        
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_location">
                                    <!-- -->   
                                    </div>
                                </div>
                            </div>
        
                            <div class="text-center">
                                {{ csrf_field() }}
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Edit Destination
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <hr>
    </div>
</div>
    <script>

////////////////////////////////////// functions //////////////////////////////////////////////////////////////

        function getAll() {
            var company_code = $("#company_code").val();
            $.get("http://localhost:8000/api/destinations/" + company_code, function(data, status){
                console.log(data, status)
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    data.forEach(element => {
                    var t = $('#example').DataTable();
                    no++;
                    t.row.add( [
                        "<center>"+no+"</center>",
                        "<center>"+element.code+"</center>",
                        "<center>"+element.name+"</center>",
                        "<center>"+element.location+"</center>",
                        "<center><a id='btn_delete' data-id="+element.id+" class='btn btn-danger'><i class='fa fa-trash'></i></a><a id='btn_edit' data-idedit="+element.id+" class='btn btn-info' data-toggle='modal' data-target='#editDestination'><i class='fa fa-pencil'></i></a></center>"
                    ] ).draw( false );
                    });
                }
                else {
                    alert('Load Data Failed')
                }
            });
        }

        function clearTable() {
            var table = $('#example').DataTable();
             table
                .clear()
                .draw();
        }

 /////////////////////////////////////// document ready functions ///////////////////////////////////////////////

        $(document).ready(function() {
            $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
                ],
            } );

            
            getAll()

            
            $(document).on('submit', '#add' ,function(){
                var company_code     = $('#company_code').val();
                var code             = $('#code').val();
                var name             = $('#name').val();
                var location         = $('#location').val();

                $.post("http://localhost:8000/api/insert-destination", {
                        company_code: company_code,
                        code: code,
                        name: name,
                        location: location
                    },
                    function(data, status){
                    if (status) {
                        toastr.success('Insert Data Success')
                        clearTable()
                        $('#addDestination').modal('hide')

                        $('#code').val('');
                        $('#name').val('');
                        $('#location').val('');
                        getAll()
                    }
                    else {
                        toastr.danger('Insert Data Failed!')
                    }
                });
            });


            $(document).on('click', '#btn_edit' ,function(){
                var id = $(this).data("idedit");
                console.log(id)
                $.get("http://localhost:8000/api/destination/" + id, function(data, status){
                console.log(data, status)
                if (status) {
                    data.forEach(element => {
                    $('#input_id').html("")
                        $('#input_id').append("<input type='hidden' required disabled id='id_edit' class='form-control border-input' placeholder='Id' value="+element.id+">");
                    $('#input_company').html("")
                        $('#input_company').append("<label>Company</label><input type='text' required disabled id='company_name_edit' class='form-control border-input' placeholder='Company' value="+element.company_name+">");
                    $('#input_code').html("")
                        $('#input_code').append("<label>Warehouse Code</label><input type='text' required id='code_edit' class='form-control border-input' placeholder='code' value="+element.code+">");
                    $('#input_name').html("")
                        $('#input_name').append("<label>Warehouse Name</label><input type='text' required id='name_edit' class='form-control border-input' placeholder='name' value="+element.name+">");
                    $('#input_location').html("")
                        $('#input_location').append("<label>City Location</label><input type='text' required id='location_edit' class='form-control border-input' placeholder='location' value="+element.location+">");
                    });
                }
                else {
                    toastr.danger('Load Data Failed!')
                    }
                });
            });     

            
            $(document).on('click', '#btn_delete' ,function(){
                // console.log('tes')
                var id = $(this).data("id");
                jQuery.ajax({
                url: 'http://localhost:8000/api/delete-destination/' + id,
                type: 'DELETE',
                success: function(data) {
                    clearTable()
                    getAll();
                    toastr.info('Delete Data Success!')
                    }
                });
            });
            
            
            $(document).on('submit', '#update' ,function(){
                var id               = $('#id_edit').val();
                var code             = $('#code_edit').val();
                var name             = $('#name_edit').val();
                var location         = $('#location_edit').val();


                var data = {
                    code : code,
                    name : name,
                    location : location,
                }

                jQuery.ajax({
                url: 'http://localhost:8000/api/update-destination/' + id,
                type: 'PUT',
                data : data,
                success: function(data) {
                    clearTable()
                    $('#editDestination').modal('hide')
                    $('#id_edit').val('')
                    $('#company_name_edit').val('')
                    $('#code_edit').val('')
                    $('#name_edit').val('')
                    $('#location_edit').val('')
                    
                    getAll();
                    toastr.success('Update Data Success!')
                    }
                });

            });
            
        } );        
    </script>
@endsection