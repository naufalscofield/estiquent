<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/profile/{id}', 'EstiquentApi@getProfile');

Route::put('/update-profile/{id}', 'EstiquentApi@updateProfile');


Route::get('/users/{cc}', 'EstiquentApi@getUser');

Route::post('/insert-user', 'EstiquentApi@storeUser');

Route::post('/login', 'EstiquentApi@loginUser');

Route::get('/user/{id}', 'EstiquentApi@showUser');

Route::put('/update-user/{id}', 'EstiquentApi@updateUser');

Route::delete('/delete-user/{id}', 'EstiquentApi@destroyUser');


Route::get('/companies', 'EstiquentApi@getCompany');

Route::post('/insert-company', 'EstiquentApi@storeCompany');

Route::get('/company/{id}', 'EstiquentApi@showCompany');

Route::put('/update-company/{id}', 'EstiquentApi@updateCompany');

Route::delete('/delete-company/{id}', 'EstiquentApi@destroyCompany');


Route::get('/warehouses/{cc}', 'EstiquentApi@getWarehouse');

Route::post('/insert-warehouse', 'EstiquentApi@storeWarehouse');

Route::get('/warehouse/{id}', 'EstiquentApi@showWarehouse');

Route::put('/update-warehouse/{id}', 'EstiquentApi@updateWarehouse');

Route::delete('/delete-warehouse/{id}', 'EstiquentApi@destroyWarehouse');


Route::get('/destinations/{cc}', 'EstiquentApi@getDestination');

Route::post('/insert-destination', 'EstiquentApi@storeDestination');

Route::get('/destination/{id}', 'EstiquentApi@showDestination');

Route::put('/update-destination/{id}', 'EstiquentApi@updateDestination');

Route::delete('/delete-destination/{id}', 'EstiquentApi@destroyDestination');


Route::get('/transactions/{cc}', 'EstiquentApi@getTransaction');

Route::post('/insert-transaction', 'EstiquentApi@storeTransaction');

Route::get('/transaction/{id}', 'EstiquentApi@showTransaction');

Route::put('/update-transaction/{id}', 'EstiquentApi@updateTransaction');

Route::delete('/delete-transaction/{id}', 'EstiquentApi@destroyTransaction');


Route::get('/costs/{cc}', 'EstiquentApi@getCost');

Route::post('/insert-cost', 'EstiquentApi@storeCost');

Route::get('/cost/{id}', 'EstiquentApi@showCost');

Route::put('/update-cost/{id}', 'EstiquentApi@updateCost');

Route::delete('/delete-cost/{id}', 'EstiquentApi@destroyCost');
